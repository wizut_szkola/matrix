#include <type_traits>
#include <array>
#include <iostream>
#include <iomanip>

namespace My
{
    template<size_t N, size_t M, typename T> 
    class Matrix
    {
        public:
            
            using row_t = std::array<T,M>;

            static_assert(std::is_arithmetic<T>::value, "Type Must by arythmetic type");
            static_assert(!std::is_same<T,bool>::value, "Type can't by bool!!! ");
            static_assert((N!=0)&&(M!=0)," Error, size must by greater than 0");

            Matrix(): m_data{{}}
            {}

            template<size_t N1, size_t M1, typename T1>
            Matrix(const Matrix<N1,M1,T1> & second)noexcept(noexcept(std::declval<T&>()= T(second[0][0])))  
            : m_data{{}}
            {
                static_assert(N1<=N," Not paussible to copy, source to large(N Value)");
                static_assert(M1<=M," Not paussible to copy, source to large (M value)");
                static_assert(std::is_convertible_v<T1,T>, "Worning Type is not convertible");

                for(unsigned int i =0; i < N1 ;i++ )
                {
                    for(unsigned int j = 0; j < M1 ; j++)
                    {
                        m_data[i][j] = T(second[i][j]);
                    }
                }
            }

           friend std::ostream& operator<<(std::ostream& os, const Matrix& data)
            {

                for(size_t i =0; i < N ; i++)
                {
                    for(size_t j =0; j < M; j++)
                    {
                        os << std::setw(2) << data.m_data[i][j] << " ";
                    }
                    os << "\n";
                
                }
                
                return os; 
            }

           template<size_t N1, size_t M1, typename T1> 
           Matrix operator+(const Matrix<N1,M1,T1>& rhs)
            {
                static_assert(N1<=N," Incorrect");
                static_assert(M1<=M," Incorrect M");
  
                for(unsigned int i =0; i < N1 ;i++ )
                {
                    for(unsigned int j = 0; j < M1 ; j++)
                    {
                        m_data[i][j] += rhs[i][j];
                    }
                }

                return *this;
            }
            
            row_t& operator[](std::size_t pos)
            {
               return m_data[pos];
            }
            
            const row_t& operator[](std::size_t pos) const
            {
                return m_data[pos];
            }
            
            void insert(std::size_t x, std::size_t y, T value)
            {
                m_data[x][y]=value;
            
            }

            T& get(std::size_t posX, std::size_t posY)
            {
              return m_data[posX][posY];
            }
            
            ~Matrix(){};

    private:
            std::array<row_t,N> m_data;
            
};
}
