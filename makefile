INFOLDER=./include
OBJDIR=build
SRCDIR=./src
DESTDIR=./realase

CC=g++
CFLAGS=-Wall -Wextra -pedantic-errors -std=c++17

_DEPS= matrix.h
DEPS=$(patsubst %,$(INFOLDER)/%,$(_DEPS))

_OBJ = main.o
OBJ = $(patsubst %,$(OBJDIR)/%,$(_OBJ))

_SRC= main.cpp
SRC=$(patsubst %,$(SRCDIR)/%,$(_SRC))

$(OBJDIR)/%.o: $(SRC) $(DEPS)
	mkdir -p $(OBJDIR) $(DESTDIR)
	$(CC) -c -o $@ $< $(CFLAGS)

matrix: $(OBJ)
	$(CC) -o $(DESTDIR)/$@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -fr $(OBJDIR) $(DESTDIR)




