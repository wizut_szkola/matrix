#include <iostream>
#include <string>

#include "../include/matrix.h"

int main()
{
    std::string test = "test";

    My::Matrix<10,10,int> Matrix;

    Matrix.insert(0,1,50);
    Matrix.insert(1,2,75);

    std::cout << Matrix.get(0,1) << std::endl;  

    std::cout << Matrix[1][2] << std::endl;
    std::cout << Matrix[3][7] << std::endl;
    
    My::Matrix<10,10,float> Matrix2(Matrix);
    My::Matrix<10,10,int> Matrix3(Matrix);;
        
    std::cout << Matrix;
    std::cout << Matrix[0][1] << std::endl;
    
    Matrix3.insert(5,6,123);
    Matrix3.insert(8,6,256);

    Matrix+Matrix3;

    std::cout << Matrix << std::endl;

    return 0;
}
